const grpc = require('grpc');
const paymentCtrl = require('./controllers/v1/opay');


const PROTO_PATH = `${__dirname  }/proto/opay.proto`;
const opayProto = grpc.load(PROTO_PATH).payment;


const server = new grpc.Server();


server.addService(opayProto.Opay.service, paymentCtrl);
server.bind('0.0.0.0:50058', grpc.ServerCredentials.createInsecure());
server.start();
console.log({ status: 'RUNNING....', service: 'EYOWO OPAY' });
