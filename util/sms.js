const http = require('http');
const config = require('../config/config.js');
const querystring = require('querystring');
const https = require('https');
const smpp = require('smpp');

module.exports = {
  sendSMPP(mobileNumber, message) {
    // mobileNumnber format: 2348066953623
    const session = smpp.connect('smpp://104.199.51.72:2345');
    session.bind_transceiver({
      system_id: 'softcom',
      password: 'com123',
    }, (pdu) => {
      if (pdu.command_status === 0) {
        // Successfully bound
        session.submit_sm({
          source_addr: 'SOFTCOM',
          destination_addr: mobileNumber,
          short_message: message,
        }, (receivedPDU) => {
          // Sample PDU response
          /*
          PDU {
            command_length: 25,
            command_id: 2147483652,
            command_status: 0,
            sequence_number: 2,
            command: 'submit_sm_resp',
            message_id: '5AED6248' } */
          if (receivedPDU.command_status === 0) {
            // Message successfully sent
            console.log(receivedPDU.message_id);
          }
          console.log(receivedPDU);
        });
      }
    });
  },
  sendEbulkSMS(mobileNumber, message) {
    const phoneNumber = mobileNumber.replace('+', '');

    console.log(`SMS sent to: ${phoneNumber}`);

    const body = JSON.stringify({
      SMS: {
        auth: {
          username: process.env.EBULK_USERNAME,
          apikey: process.env.EBULK_SMS_APIKEY,
        },
        message: {
          sender: 'Eyowo',
          messagetext: message,
          flash: '0',
        },
        recipients:
          {
            gsm: [
              {
                msidn: phoneNumber,
                msgid: '1',
              },
            ],
          },
      },
    });

    const httpreq = http.request(config.smshttpoptions, (response) => {
      let str = '';
      response.on('data', (chunk) => {
        str += chunk;
      });

      response.on('end', () => {
        console.log(str);
        // sample success message: {"response":{"status":"SUCCESS","totalsent":1,"cost":1}}
        const parsed = JSON.parse(str);
        console.log(parsed); // JSON Object
      });
    });

    httpreq.end(body);
  },
  sendAfricasTalking(mobile, message) {
    const username = process.env.AFRICAS_TALKING_USERNAME;
    const apikey = process.env.AFRICAS_TALKING_API_KEY;

    // Add + character
    const to = `+${mobile}`;

    // Define the recipient numbers in a comma separated string
    // Numbers should be in international format as shown
    // const to = '+2348066953623,+2349092345679';

    // And of course we want our recipients to know what we really do
    // const message = "Your Eyowo validation code is 5678";

    // Build the post string from an object
    const postData = querystring.stringify({
      username,
      to,
      message,
      from: 'Eyowo',
    });

    const postOptions = {
      host: 'api.africastalking.com',
      path: '/version1/messaging',
      method: 'POST',
      rejectUnauthorized: false,
      requestCert: true,
      agent: false,

      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Content-Length': postData.length,
        Accept: 'application/json',
        apikey,
      },
    };

    const postReq = https.request(postOptions, (res) => {
      res.setEncoding('utf8');
      res.on('data', (chunk) => {
        console.log(chunk);
        const jsObject = JSON.parse(chunk);
        const recipients = jsObject.SMSMessageData.Recipients;
        if (recipients.length > 0) {
          for (let i = 0; i < recipients.length; i += 1) {
            let logStr = `number=${recipients[i].number}`;
            logStr += `;cost=${recipients[i].cost}`;
            logStr += `;status=${recipients[i].status}`; // status is either "Success" or "error message"
            console.log(logStr);
          }
        } else {
          console.log(`Error while sending: ${jsObject.SMSMessageData.Message}`);
        }
      });
    });
    // Add post parameters to the http request
    postReq.write(postData);
    postReq.end();
  },
  sendNew(mobile, message) {
    // this.sendAfricasTalking(mobile, message);
    this.sendEbulkSMS(`+${mobile}`, message);
  },
};

