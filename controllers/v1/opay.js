const axios = require('axios');
const mongoose = require('mongoose');
const request = require('request');
const jwt = require('jsonwebtoken');
const grpc = require('grpc');
require('dotenv').config();

const PROTO_PATH = `${__dirname}/proto/user.proto`;
const UserService = grpc.load(PROTO_PATH).eyowo_user;
const UserRPC = new UserService.User('35.232.39.202:80', grpc.credentials.createInsecure());


const InitPayment = async (call, callback) => {
  // console.log(call);
  try {
    const req = call.request;

    const { amount, mobile } = req;

    const response = await axios.post('https://operapay.com/api/gateway/create', {
      input: {
        currency: 'NGN',
        publicKey: process.env.opay_public,
        amount: req.amount,
        reference: mongoose.Types.ObjectId().toString(),
        countryCode: 'NG',
        tokenize: true,
        instrumentType: 'card',
        cardNumber: req.cardNumber,
        cardDateMonth: req.cardDateMonth,
        cardDateYear: req.cardDateYear,
        cardCVC: req.cardCVC,
      },
    });

    // console.log(response);

    const { data } = response;
    const r = data.data.gatewayCreate;

    if (!r) {
      const error = data.errors[0].message;
      console.log(error);
      return callback(error);
    }

    console.log(r.token);
    const jtoken = jwt.sign({
      mobile, amount, token: r.token, card: req.cardNumber,
    }, 'cardsecret');

    if (r.status === 'successful') {
      const card = {
        pan1: req.cardNumber.substr(0, 4),
        pan2: req.cardNumber.substr(-4),
        instrumentId: r.instrumentId,
      };

      // call cardFundWallet
      UserRPC.creditUserWithCard({ amount, card, mobile }, (err, resp) => {
        if (err) throw err;
        console.log(resp);
      });
    } else if (r.status === 'failed') {
      return callback(null, { success: false, data: JSON.stringify(r) });
    } else {
      r.token = jtoken;
      return callback(null, { success: true, data: JSON.stringify(r) });
    }
  } catch (err) {
    console.log(err);
    return callback(err);
  }
};

const InputPin = async (call, callback) => {
  try {
    const req = call.request;

    console.log(req);
    const token = jwt.verify(String(req.token), 'cardsecret');

    const response = await axios.post('https://operapay.com/api/gateway/input-pin', {
      token: token.token,
      pin: req.pin,
    });

    const { data } = response;
    const r = data.data.gatewayInputPIN;

    if (!r) {
      const error = data.errors[0].message;

      return callback(null, ({ success: false, data: JSON.stringify(error) }));
    }

    console.log('input-pin', r);
    const jtoken = jwt.sign({
      mobile: token.mobile, amount: token.amount, token: token.token, card: token.card,
    }, 'cardsecret');


    if (r.status === 'successful') {
      const card = {
        pan1: token.card.substr(0, 4),
        pan2: token.card.substr(-4),
        instrumentId: r.instrumentId,
      };

      // call cardFundWallet
      UserRPC.creditUserWithCard({ amount: token.amount, card, mobile: token.mobile }, (err, resp) => {
        if (err) throw err;
        console.log(resp);
        console.log(err);
      });
    } else if (r.status === 'failed') {
      return callback(null, { success: false, data: 'Transaction failed' });
    } else {
      r.token = jtoken;
      return callback(null, { success: true, data: JSON.stringify(r) });
    }
  } catch (err) {
    console.log(err);
    return callback(null, { success: false, data: 'error  Initializing Data' });
  }
};

const InputOtp = async (call, callback) => {
  try {
    const req = call.request;
    const token = jwt.verify(String(req.token), 'cardsecret');
    console.log(token, req.otp);
    const response = await axios.post('https://operapay.com/api/gateway/input-otp',
      {
        token: token.token,
        otp: req.otp,
      });
    const { data } = response;


    const r = data.data.gatewayInputOTP;
    if (!r) {
      const error = data.errors[0].message;

      return callback(null, ({ success: false, data: JSON.stringify(error) }));
    }
    console.log('input-otp', r);
    if (r.status === 'successful') {
      const card = {
        pan1: token.card.substr(0, 4),
        pan2: token.card.substr(-4),
        instrumentId: r.instrumentId,
      };
     //UPDATE USER EYOWO WALLET AND ADD INTRUMENT ID 
      console.log(r);
      return callback(null, { success: true, data: JSON.stringify(r) });
    } if (r.status === 'failed') {
      return callback(null, { success: false, data: JSON.stringify(r) });
    }
    return callback(null, { success: true, data: JSON.stringify(r) });
  } catch (err) {
    console.log(err);
    return callback(null, { success: false, data: 'error  Initializing Data' });
  }
};


const getAccessCode = async () => new Promise((resolve) => {
  request.post('https://operapay.com/api/access/code', {
    json: true,
    body: {
      publicKey: process.env.opay_public,
    },
  }, (err, response, body) => {
    resolve(body.data.accessCode);
  });
});


const getToken = async () => new Promise(async (resolve) => {
  request.post('https://operapay.com/api/access/token', {
    json: true,
    body: {
      accessCode: await getAccessCode(),
      privateKey: process.env.opay_private,
    },
  }, (err, response, body) => resolve(body.data.accessToken.value));
});


const ChargeIntrumentId = async (call, callback) => {
  const req = call.request;
  request.post({
    url: 'https://operapay.com/api/payment/order',
    json: true,
    headers: { Authorization: await getToken() },
    body: {
      orderConfig: {
        serviceType: 'gateway',
        paymentAmount: req.amount,
        currencyISO: 'NGN',
        countryCode: 'NG',
        instruments: [{ type: 'token', id: req.id }],
      },
    },
  }, (err, response, body) => {
    const r = body.data.orderForMerchant;
    console.log(body);
    if (!r) {
      const error = body.errors[0].message;

      return callback(null, ({ success: false, data: JSON.stringify(error) }));
    }

    return callback(null, ({ success: true, data: JSON.stringify(r) }));
  });
};


const TransferToBank = async (call, callback) => {
  try {
    const req = call.request;
    request.post('https://operapay.com/api/payment/order', {
      json: true,
      headers: {
        Authorization: `token ${await getToken()}`,
      },
      body: {
        orderConfig: {
          serviceType: 'bank',
          recipientAccount: req.account_number,
          recipientBankCode: req.bank_code,
          paymentAmount: req.amount,
          currencyISO: 'NGN',
          countryCode: 'NG',
          customerPhone: req.mobile,
          customerEmail: 'customer@email.com',
          instruments: [{
            type: 'coins',
          }],
        },
      },
    }, (err, response, body) => {
      const r = body.data.orderForMerchant;
      if (!r) {
        const error = body.errors[0].message;

        return callback(null, ({ success: false, data: JSON.stringify(error) }));
      }
      return callback(null, ({ success: true, data: JSON.stringify(r) }));
    });
  } catch (err) {
    return callback(null, { success: false, data: 'error  Initializing Data' });
  }
};


module.exports = {
  InitPayment, InputOtp, InputPin, ChargeIntrumentId, TransferToBank,
};
